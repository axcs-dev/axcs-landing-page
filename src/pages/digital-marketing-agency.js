import React from "react";
import Layout from "../components/App/Layout";
import Navbar from "../components/App/Navbar";
import Footer from "../components/App/Footer";
import MainBanner from "../components/DigitalMarketingAgency/MainBanner";
import FeaturedServices from "../components/DigitalMarketingAgency/FeaturedServices";
import Partner from "../components/DigitalMarketingAgency/Partner";
import AboutUs from "../components/DigitalMarketingAgency/AboutUs";
import Services from "../components/DigitalMarketingAgency/Services";
import FunFacts from "../components/DigitalMarketingAgency/FunFacts";
import Projects from "../components/DigitalMarketingAgency/Projects";
import Testimonials from "../components/DigitalMarketingAgency/Testimonials";
import StartProject from "../components/DigitalMarketingAgency/StartProject";
import OurBlog from "../components/DigitalMarketingAgency/BlogPost";

const Index = () => {
  return (
    <Layout>
      <Navbar />
      <MainBanner />
      <FeaturedServices />
      <Partner />
      <AboutUs />
      <Services />
      <FunFacts />
      <Projects />
      <Testimonials />
      <StartProject />
      <OurBlog />
      <Footer />
    </Layout>
  );
};

export default Index;
