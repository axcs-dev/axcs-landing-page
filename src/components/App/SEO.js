// import React from 'react';
// import { Helmet } from 'react-helmet';
// import { useStaticQuery, graphql } from 'gatsby';

// const SEO = ({ post }) => {
//   const data = useStaticQuery(graphql`
//     {
//       site {
//         siteMetadata {
//           title
//           description
//           baseUrl
//         }
//       }
//     }
//   `);

//   const defaults = data.site.siteMetadata;

//   // if (defaults.baseUrl === '' && typeof window !== 'undefined') {
//   //   defaults.baseUrl = window.location.origin;
//   // }

//   // if (defaults.baseUrl === '') {
//   //   console.error('Please set a baseUrl in your site metadata!');
//   //   return null;
//   // }

//   const title = post.title || defaults.title;
//   const description = post.description || defaults.description;
//   const url = new URL(post.path || '', defaults.baseUrl);
//   const image = post.image ? new URL(post.image, defaults.baseUrl) : false;

//   return (
//     <Helmet>
//       <meta property="fb:app_id" content="193502672244370"/>
//       <meta property="og:url" content="https://axcs.io"/>
//       <meta property="og:type" content="website"/>
//       <meta property="og:title" content="Axcs - The exclusive content subscription platform"/>
//       <meta property="og:description" content="A better version of OnlyFans. Content creators can earn money from users who subscribe to their content.
//       Axcs allows content creators to receive funding directly from their fans on a monthly basis as well as one-time tips."/>
//       <meta property="og:image" content="https://i.imgur.com/rVo3qXg.png"/>
//       <meta property="og:site_name" content="Axcs"/>

//       <meta name="twitter:card" content="summary_large_image"/>
//       <meta name="twitter:site" content="@Axcs"/>
//       <meta name="twitter:creator" content="@KingCaleb3"/>
//       <meta name="twitter:title" content="Axcs - The exclusive content subscription platform"/>
//       <meta name="twitter:description" content="A better version of OnlyFans. Content creators can earn money from users who subscribe to their content.
//       Axcs allows content creators to receive funding directly from their fans on a monthly basis as well as one-time tips."/>
//       <meta name="twitter:image" content="https://i.imgur.com/rVo3qXg.png"/>
//     </Helmet>
//   );
// };

// export default SEO;