import React from "react";
import { Link } from "gatsby";
import projectStart from "../../assets/images/project-start1.svg";
import shape from "../../assets/images/shape/circle-shape1.png";

const ProjectStartArea = () => {
  return (
    <div className="project-start-area ptb-100">
      <div className="container">
        <div className="row align-items-center">
          <div className="col-lg-6 col-md-12">
            <div className="project-start-image">
              <img src={projectStart} alt="project" />
            </div>
          </div>

          <div className="col-lg-6 col-md-12">
            <div className="project-start-content">
              <h2>Sign Up Today to Start Monetizing your Influence</h2>
              <p>
                Do you have an idea of what you want to achieve,
                but not sure how to accomplish it? Most of us have dreams
                and setting goals is a great way to help you fuel your ambitions
              </p>

              <Link to="/contact" className="default-btn">
                <i className="flaticon-web"></i>
                Get Started
                <span></span>
              </Link>
            </div>
          </div>
        </div>
      </div>

      <div className="circle-shape1">
        <img src={shape} alt="project" />
      </div>
    </div>
  );
};

export default ProjectStartArea;
