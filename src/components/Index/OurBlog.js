import React from "react";
import { Link } from "gatsby";
import starIcon from "../../assets/images/star-icon.png";
import blog1 from "../../assets/images/blog/blog-img1.svg";
import blog5 from "../../assets/images/blog/blog-img5.svg";
import blog6 from "../../assets/images/blog/blog-img6.svg";
import user1 from "../../assets/images/user1.jpg";
import user2 from "../../assets/images/user2.jpg";
import user3 from "../../assets/images/user3.jpg";

const OurBlog = () => {
  return (
    <section className="blog-area pt-100 pb-70 bg-fffbf5">
      <div className="container">
        <div className="section-title">
          <span className="sub-title">
            <img src={starIcon} alt="blog" />
            Our Blog
          </span>
          <h2>Latest Valuable Insights</h2>
          <p>
            For many content creators with different levels of influence on social media,
            the ability to turn an online brand into a success story is a major step.
          </p>
        </div>

        <div className="row">
          <div className="col-lg-4 col-md-6">
            <div className="single-blog-post">
              <div className="post-image">
                <Link to="/blog-details">
                  <img src={blog1} alt="blog" />
                </Link>
              </div>

              <div className="post-content">
                <ul className="post-meta d-flex justify-content-between align-items-center">
                  <li>
                    <div className="post-author d-flex align-items-center">
                      <img src={user1} className="rounded-circle" alt="blog" />
                      <span>Alex Morgan</span>
                    </div>
                  </li>
                  <li>
                    <i className="flaticon-calendar"></i> April 30, 2021
                  </li>
                </ul>
                <h3>
                  <Link to="/blog-details">
                    What Can I Live Stream?
                  </Link>
                </h3>
              </div>
            </div>
          </div>

          <div className="col-lg-4 col-md-6">
            <div className="single-blog-post">
              <div className="post-image">
                <Link to="/blog-details">
                  <img src={blog5} alt="blog" />
                </Link>
              </div>

              <div className="post-content">
                <ul className="post-meta d-flex justify-content-between align-items-center">
                  <li>
                    <div className="post-author d-flex align-items-center">
                      <img src={user2} className="rounded-circle" alt="blog" />
                      <span>Sarah Taylor</span>
                    </div>
                  </li>
                  <li>
                    <i className="flaticon-calendar"></i> April 28, 2021
                  </li>
                </ul>
                <h3>
                  <Link to="/blog-details">
                    Creative Photography Techniques: Tips For Photographers
                  </Link>
                </h3>
              </div>
            </div>
          </div>

          <div className="col-lg-4 col-md-6 offset-lg-0 offset-md-3">
            <div className="single-blog-post">
              <div className="post-image">
                <Link to="/blog-details">
                  <img src={blog6} alt="blog" />
                </Link>
              </div>

              <div className="post-content">
                <ul className="post-meta d-flex justify-content-between align-items-center">
                  <li>
                    <div className="post-author d-flex align-items-center">
                      <img src={user3} className="rounded-circle" alt="blog" />
                      <span>David Warner</span>
                    </div>
                  </li>
                  <li>
                    <i className="flaticon-calendar"></i> April 29, 2021
                  </li>
                </ul>
                <h3>
                  <Link to="/blog-details">
                    Setting yourself goals on Axcs
                  </Link>
                </h3>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default OurBlog;
