import React from "react";
import { Link } from "gatsby";
import starIcon from "../../assets/images/star-icon.png";
import project1 from "../../assets/images/projects/project1.svg";
import project2 from "../../assets/images/projects/project2.svg";
import project3 from "../../assets/images/projects/project3.svg";
import project4 from "../../assets/images/projects/project4.svg";
import project5 from "../../assets/images/projects/project5.svg";
import project6 from "../../assets/images/projects/project6.svg";

const RecentProjects = () => {
  return (
    <section className="projects-area bg-color pt-100 pb-70">
      <div className="container">
        <div className="section-title">
          <span className="sub-title">
            <img src={starIcon} alt="project" /> Recent Projects
          </span>
          <h2>Check Some Of Our Recent Work</h2>
          <p>
            It’s time to ‘stir the drink’ as your favorite creators might say 💁‍♀️
            If you’re looking for exclusive content and behind the scenes gold
            then look no further! 🤩
          </p>
        </div>

        <div className="row">
          <div className="col-lg-4 col-md-6">
            <div className="single-projects-box">
              <div className="image">
                <img src={project1} alt="project" />

                <Link className="link-btn" to="/case-studies-details">
                  <i className="bx bx-plus"></i>
                </Link>
              </div>

              <div className="content">
                <h3>
                  <Link to="/case-studies-details">Why start an Axcs Page?</Link>
                </h3>
                <span>Financial</span>
              </div>
            </div>
          </div>

          <div className="col-lg-4 col-md-6">
            <div className="single-projects-box">
              <div className="image">
                <img src={project2} alt="project" />

                <Link className="link-btn" to="/case-studies-details">
                  <i className="bx bx-plus"></i>
                </Link>
              </div>

              <div className="content">
                <h3>
                  <Link to="/case-studies-details">Rising Stars on Axcs</Link>
                </h3>
                <span>Social Media</span>
              </div>
            </div>
          </div>

          <div className="col-lg-4 col-md-6">
            <div className="single-projects-box">
              <div className="image">
                <img src={project3} alt="project" />

                <Link className="link-btn" to="/case-studies-details">
                  <i className="bx bx-plus"></i>
                </Link>
              </div>

              <div className="content">
                <h3>
                  <Link to="/case-studies-details">Live Streaming</Link>
                </h3>
                <span>Branding</span>
              </div>
            </div>
          </div>

          <div className="col-lg-4 col-md-6">
            <div className="single-projects-box">
              <div className="image">
                <img src={project4} alt="project" />

                <Link className="link-btn" to="/case-studies-details">
                  <i className="bx bx-plus"></i>
                </Link>
              </div>

              <div className="content">
                <h3>
                  <Link to="/case-studies-details">Being Creative</Link>
                </h3>
                <span>Content</span>
              </div>
            </div>
          </div>

          <div className="col-lg-4 col-md-6">
            <div className="single-projects-box ">
              <div className="image">
                <img src={project5} alt="project" />

                <Link className="link-btn" to="/case-studies-details">
                  <i className="bx bx-plus"></i>
                </Link>
              </div>

              <div className="content">
                <h3>
                  <Link to="/case-studies-details">Setting Goals</Link>
                </h3>
                <span>Business</span>
              </div>
            </div>
          </div>

          <div className="col-lg-4 col-md-6">
            <div className="single-projects-box">
              <div className="image">
                <img src={project6} alt="project" />

                <Link className="link-btn" to="/case-studies-details">
                  <i className="bx bx-plus"></i>
                </Link>
              </div>

              <div className="content">
                <h3>
                  <Link to="/case-studies-details">Beauty Creators</Link>
                </h3>
                <span>Content</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default RecentProjects;
