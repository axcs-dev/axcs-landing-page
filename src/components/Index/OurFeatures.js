import React from "react";
import starIcon from "../../assets/images/star-icon.png";
import serviceIcon1 from "../../assets/images/services/service-icon1.png";
import serviceIcon2 from "../../assets/images/services/service-icon2.png";
import serviceIcon3 from "../../assets/images/services/service-icon3.png";
import serviceIcon4 from "../../assets/images/services/service-icon4.png";
import serviceIcon5 from "../../assets/images/services/service-icon5.png";
import serviceIcon6 from "../../assets/images/services/service-icon6.png";

const OurFeatures = () => {
  return (
    <section className="services-area pt-100 pb-70 bg-f1f8fb">
      <div className="container">
        <div className="section-title">
          <span className="sub-title">
            <img src={starIcon} alt="feature" />
            Our Features
          </span>

          <h2>Make Money Today</h2>
          <p>
            It's a great time to be a content creator. After a year of lockdown,
            niche content is more popular than ever before!
          </p>
        </div>

        <div className="row">
          <div className="col-lg-4 col-sm-6">
            <div className="single-services-item-box">
              <div className="icon">
                <img src={serviceIcon1} alt="feature" />
              </div>
              <h3>Incredible Infrastructure</h3>
              <p>
                Our platform was built using modern technology. We are flexible,
                scalable, adaptable and ready to serve our content creators.
              </p>
            </div>
          </div>

          <div className="col-lg-4 col-sm-6">
            <div className="single-services-item-box">
              <div className="icon">
                <img src={serviceIcon2} alt="feature" />
              </div>
              <h3>Smart Notifications</h3>
              <p>
                Getting notified or notifying your follownig. Either way we made
                communication with your user as easy as possible.
              </p>
            </div>
          </div>

          <div className="col-lg-4 col-sm-6">
            <div className="single-services-item-box">
              <div className="icon">
                <img src={serviceIcon3} alt="feature" />
              </div>
              <h3>A.I. Dashboard</h3>
              <p>
                With our smart dashboard, you'll be able to take your content
                and turn them into money making assets immediately.
              </p>
            </div>
          </div>

          <div className="col-lg-4 col-sm-6">
            <div className="single-services-item-box">
              <div className="icon">
                <img src={serviceIcon4} alt="feature" />
              </div>
              <h3>Improved Search</h3>
              <p>
                Part of being a content creator is being found. Our tool gives
                creators the option to be visible to all users or only a
                selected bunch.
              </p>
            </div>
          </div>

          <div className="col-lg-4 col-sm-6">
            <div className="single-services-item-box">
              <div className="icon">
                <img src={serviceIcon5} alt="feature" />
              </div>
              <h3>Marketplace</h3>
              <p>
                The marketplace aka “explore” page helps users find other
                creators as well as the content they are trying to promote or
                sell.
              </p>
            </div>
          </div>

          <div className="col-lg-4 col-sm-6">
            <div className="single-services-item-box">
              <div className="icon">
                <img src={serviceIcon6} alt="feature" />
              </div>
              <h3>Complete Scheduling</h3>
              <p>
                Creators are able to interact with their following as well as
                schedule certain events that they can hold via Live Stream.
              </p>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default OurFeatures;
