import React from "react";
import { Link } from "gatsby";
import starIcon from "../../assets/images/star-icon.png";

const OurSolutions = () => {
  return (
    <section className="solutions-area pb-70">
      <div className="container">
        <div className="section-title">
          <span className="sub-title">
            <img src={starIcon} alt="star" />
            Our Solutions
          </span>
          <h2>Why Get Axcs?</h2>
          <p>
            We looked at the competition and realize we could do better. We
            offer a unique user experience, combining your favorite features
            into one app. Everything we build is focused on empowering the
            creator.
          </p>
        </div>

        <div className="row">
          <div className="col-lg-4 col-sm-6">
            <div className="single-solutions-box">
              <div className="icon">
                <i className="flaticon-rocket"></i>
              </div>
              <h3>
                <Link to="/service-details">Monetization Potential</Link>
              </h3>
              <p>
                What's better than empowering a creator than putting money in
                their pocket. Start uploading today and grow your reach and
                influence. You get a better deal with us.
              </p>

              <Link to="/service-details" className="view-details-btn">
                View Details
              </Link>
            </div>
          </div>

          <div className="col-lg-4 col-sm-6">
            <div className="single-solutions-box">
              <div className="icon">
                <i className="flaticon-laptop"></i>
              </div>

              <h3>
                <Link to="/service-details">Private Content</Link>
              </h3>

              <p>
                We use blockchain and AI solutions to protect your content.
                You will not have to worry about the security of your
                photos, videos, blogs, etc.
              </p>

              <Link to="/service-details" className="view-details-btn">
                View Details
              </Link>
            </div>
          </div>

          <div className="col-lg-4 col-sm-6 offset-lg-0 offset-sm-3">
            <div className="single-solutions-box">
              <div className="icon">
                <i className="flaticon-money"></i>
              </div>

              <h3>
                <Link to="/service-details">Full Control</Link>
              </h3>

              <p>
                Unlike other social media platforms, Axcs gives the creator the
                ability to control their level of access.
                Our AI will filter out the disrespectful messages you don't want to see.
              </p>

              <Link to="/service-details" className="view-details-btn">
                View Details
              </Link>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default OurSolutions;
