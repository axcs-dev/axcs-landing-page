import React from "react";
import service1 from "../../assets/images/services/service1.png";
import service2 from "../../assets/images/services/service2.png";
import starIcon from "../../assets/images/star-icon.png";

const OurServices = () => {
  return (
    <React.Fragment>
      {/* Service Left Image Style */}
      <div className="about-area pb-100">
        <div className="container-fluid">
          <div className="row align-items-center">
            <div className="col-lg-6 col-md-12">
              <div className="about-img">
                <img src={service1} alt="service" />
              </div>
            </div>

            <div className="col-lg-6 col-md-12">
              <div className="about-content">
                <div className="content">
                  <span className="sub-title">
                    <img src={starIcon} alt="icon" /> Features
                  </span>

                  <h2>Our Features</h2>
                  <p>
                    You’ve probably been wondering how a lot of influencers are
                    monetizing and growing their online presence so quickly. The
                    answer for many of them is signing up as a content creator.
                  </p>
                  <ul className="about-list mb-0">
                    <li>
                      <i className="flaticon-tick"></i>
                      Free to Join
                    </li>
                    <li>
                      <i className="flaticon-tick"></i>
                      Easy to Start
                    </li>
                    <li>
                      <i className="flaticon-tick"></i>
                      Safe to Use
                    </li>
                    <li>
                      <i className="flaticon-tick"></i>
                      Montetize today
                    </li>
                    <li>
                      <i className="flaticon-tick"></i>
                      Secure Platform
                    </li>
                    <li>
                      <i className="flaticon-tick"></i>
                      Contol Your Content
                    </li>
                    <li>
                      <i className="flaticon-tick"></i>
                      Paid Messages
                    </li>
                    <li>
                      <i className="flaticon-tick"></i>
                      Creator Social Page
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* End Service Left Image Style */}

      {/* Service Right Image Style */}
      <div className="our-mission-area pb-100">
        <div className="container-fluid">
          <div className="row align-items-center">
            <div className="col-lg-6 col-md-12">
              <div className="our-mission-content">
                <div className="content">
                  <span className="sub-title">
                    <img src={starIcon} alt="icon" /> Services
                  </span>

                  <h2>How We Are Different</h2>
                  <p>
                    You might be asking. Why not use one of the other content
                    platforms? There is OnlyFans and JustForFans but we offer
                    the best of both worlds with some of the latest technology
                    avaliable.
                  </p>
                  <ul className="our-mission-list mb-0">
                    <li>
                      <i className="flaticon-tick"></i>
                      Payment Percentage
                    </li>
                    <li>
                      <i className="flaticon-tick"></i>
                      Better Search
                    </li>
                    <li>
                      <i className="flaticon-tick"></i>
                      Live Streaming
                    </li>
                    <li>
                      <i className="flaticon-tick"></i>
                      Sell Individual Clips
                    </li>
                    <li>
                      <i className="flaticon-tick"></i>
                      Improved Tipping
                    </li>
                    <li>
                      <i className="flaticon-tick"></i>
                      Pay Per View Texting
                    </li>
                    <li>
                      <i className="flaticon-tick"></i>
                      Referrals
                    </li>
                    <li>
                      <i className="flaticon-tick"></i>
                      Audience Appeal
                    </li>
                  </ul>
                </div>
              </div>
            </div>

            <div className="col-lg-6 col-md-12">
              <div className="our-mission-image">
                <img src={service2} alt="service" />
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* End Service Right Image Style */}
    </React.Fragment>
  );
};

export default OurServices;
