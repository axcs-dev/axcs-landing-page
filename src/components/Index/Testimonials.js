import React from "react";
import { Link } from "gatsby";
import starIcon from "../../assets/images/star-icon.png";
import client1 from "../../assets/images/testimonials/client1.jpg";
import client2 from "../../assets/images/testimonials/client2.jpg";
import client3 from "../../assets/images/testimonials/client3.jpg";
import shape from "../../assets/images/shape/shape1.svg";
import Loadable from "@loadable/component";
const OwlCarousel = Loadable(() => import("react-owl-carousel3"));

const options = {
  loop: true,
  nav: true,
  dots: false,
  autoplayHoverPause: true,
  autoplay: true,
  margin: 30,
  navText: [
    "<i class='flaticon-left-1'></i>",
    "<i class='flaticon-right-1'></i>",
  ],
  responsive: {
    0: {
      items: 1,
    },
    768: {
      items: 2,
    },
    992: {
      items: 2,
    },
  },
};

const Testimonials = () => {
  const [display, setDisplay] = React.useState(false);

  React.useEffect(() => {
    setDisplay(true);
  }, []);

  return (
    <div className="testimonials-area pt-100 bg-f1f8fb">
      <div className="container">
        <div className="section-title">
          <span className="sub-title">
            <img src={starIcon} alt="testimonial" />
            Testimonials
          </span>
          <h2>What Our Clients are Saying?</h2>
          <p>
            It was a pleasure working with the entire Axcs development team.
            They were able to help me launch page instantly and it looks and works beautifully.
            It's difficult to find a platform with a balance of business and tech support.
            Axcs platform is so easy to use, I was able to start sharing content immediately.
          </p>
        </div>

        {display ? (
          <OwlCarousel
            className="testimonials-slides owl-carousel owl-theme"
            {...options}
          >
            <div className="single-testimonials-item">
              <p>
                Trying to setup a paywal for my content was frustrating. Options were either too expensive or not good enough.
                It was a frustrating process until I found to Axcs. I had talked to at least 6 different companies
                and individuals who treated me like I was a bother more than a potential client. Axcs has been
                wonderful! They are professional, hard working and a pleasure to work with!
              </p>

              <div className="client-info">
                <div className="d-flex justify-content-center align-items-center">
                  <img src={client1} alt="testimonial" />
                  <div className="title">
                    <h3>Alex Maxwell</h3>
                    <span>CEO at EnvyTheme</span>
                  </div>
                </div>
              </div>
            </div>

            <div className="single-testimonials-item">
              <p>
                We’ve been very pleased with the platform that Axcs has developed. Historically, we’ve kept all
                social media like Instagram, but on a whim we decided to give Axcs a chance.
                There app is really fast! We love using the Axcs platform to provide our content.
              </p>

              <div className="client-info">
                <div className="d-flex justify-content-center align-items-center">
                  <img src={client2} alt="testimonial" />
                  <div className="title">
                    <h3>David Warner</h3>
                    <span>CEO at Envato</span>
                  </div>
                </div>
              </div>
            </div>

            <div className="single-testimonials-item">
              <p>
                Working with Axcs was an exceptional experience. I've tried other platforms like OnlyFans but they are
                over crowded and have little customer support.
              </p>
              <div className="client-info">
                <div className="d-flex justify-content-center align-items-center">
                  <img src={client3} alt="testimonial" />
                  <div className="title">
                    <h3>Sarah Taylor</h3>
                    <span>CEO at ThemeForest</span>
                  </div>
                </div>
              </div>
            </div>
          </OwlCarousel>
        ) : (
          ""
        )}

        <div className="testimonials-view-btn text-center">
          <Link to="/testimonials" className="default-btn">
            <i className="flaticon-view"></i>
            Check Out All Reviews <span></span>
          </Link>
        </div>
      </div>

      <div className="shape-img1">
        <img src={shape} alt="testimonial" />
      </div>
    </div>
  );
};

export default Testimonials;
